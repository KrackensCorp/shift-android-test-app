package ru.shiftlaboratory.shiftregistration.ext

import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import ru.shiftlaboratory.shiftregistration.data.ValidateState

fun AppCompatActivity.validateObserver(liveData: LiveData<ValidateState>, editText: EditText) {
    liveData.observe(this, Observer {
        when (it) {
            is ValidateState.Error -> editText.error = getString(it.errorMessage)
            ValidateState.Success -> editText.error = null
        }
    })
}

fun Button.validateObserver(owner: LifecycleOwner, liveData: LiveData<Boolean>) {
    liveData.observe(owner, Observer {
        isEnabled = it
    })
}