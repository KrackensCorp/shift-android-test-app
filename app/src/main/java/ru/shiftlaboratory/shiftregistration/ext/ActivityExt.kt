package ru.shiftlaboratory.shiftregistration.ext

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

inline fun <reified C : AppCompatActivity> AppCompatActivity.navigate(action: Intent.() -> Unit = {}) {
    startActivity(Intent(this, C::class.java).apply(action))
    finish()
}