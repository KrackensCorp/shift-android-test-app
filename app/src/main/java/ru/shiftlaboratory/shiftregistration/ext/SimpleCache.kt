package ru.shiftlaboratory.shiftregistration.ext

import android.content.Context
import androidx.core.content.edit
import ru.shiftlaboratory.shiftregistration.data.model.User

class SimpleCache(private val context: Context) {

    private companion object {
        const val PREFERENCE_NAME = "PREFERENCE_NAME"
        const val NAME = "NAME"
        const val SURNAME = "SURNAME"
        const val DATE = "DATE"
        const val ID = "ID"
        const val PASSWORD = "PASSWORD"
    }

    fun setData(data: User) {
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit {
            putInt(ID, data.id)
            putString(NAME, data.name)
            putString(SURNAME, data.surname)
            putString(DATE, data.date)
            putString(PASSWORD, data.password)
            commit()
        }
    }

    fun existData(): Boolean {
        with(context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)) {
            listOf(ID, NAME, SURNAME, DATE, PASSWORD).forEach {
                if (!contains(it))
                    return false
            }
        }
        return true
    }

    fun getData(): User? {
        with(context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)) {
            val id = getInt(ID, -1)
            val name = getString(NAME, null)
            val surname = getString(SURNAME, null)
            val date = getString(DATE, null)
            val pass = getString(PASSWORD, null)
            return if (
                id != 1
                && !name.isNullOrBlank()
                && !surname.isNullOrBlank()
                && !date.isNullOrBlank()
                && !pass.isNullOrBlank()
            ) {
                User(
                    id = id,
                    name = name,
                    surname = surname,
                    date = date,
                    password = pass
                )
            } else {
                null
            }

        }
    }

    fun clearData() {
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit { clear() }
    }
}