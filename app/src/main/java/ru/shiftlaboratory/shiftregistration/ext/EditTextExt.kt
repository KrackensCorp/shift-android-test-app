package ru.shiftlaboratory.shiftregistration.ext

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.lifecycle.MutableLiveData

fun EditText.afterTextChanged(value: MutableLiveData<String>, action: () -> Unit = {}) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            value.value = editable.toString().also { Log.d("afterTextChanged", it) }
            action()
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}