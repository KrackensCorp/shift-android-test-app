package ru.shiftlaboratory.shiftregistration.ui.info

import androidx.lifecycle.ViewModel
import ru.shiftlaboratory.shiftregistration.data.model.User

class InfoViewModel : ViewModel() {
    lateinit var data: User
}