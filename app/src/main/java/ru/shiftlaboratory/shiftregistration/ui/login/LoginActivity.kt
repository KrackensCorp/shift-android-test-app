package ru.shiftlaboratory.shiftregistration.ui.login

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_login.*
import ru.shiftlaboratory.shiftregistration.R
import ru.shiftlaboratory.shiftregistration.ext.SimpleCache
import ru.shiftlaboratory.shiftregistration.ext.afterTextChanged
import ru.shiftlaboratory.shiftregistration.ext.navigate
import ru.shiftlaboratory.shiftregistration.ext.validateObserver
import ru.shiftlaboratory.shiftregistration.ui.info.InfoActivity
import ru.shiftlaboratory.shiftregistration.ui.login.uistate.LoginUIState
import java.util.*


class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel

    private val cache by lazy { SimpleCache(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        setObserverState()
        setTextChangedListeners()
        checkData()

        dateTv.setOnClickListener {
            showDatePicker()
        }

        login.setOnClickListener {
            viewModel.login()
        }
    }

    private fun checkData() {
		if (cache.existData()) {
			navigate<InfoActivity>()
		}
    }

    private fun setTextChangedListeners() {
        name.afterTextChanged(viewModel.name)
        surname.afterTextChanged(viewModel.surname)
        password.afterTextChanged(viewModel.pass)
        repeatPassword.afterTextChanged(viewModel.passRepeat)
        dateTv.afterTextChanged(viewModel.date)
    }

    private fun setObserverState() {
        validateObserver(viewModel.validateName, name)
        validateObserver(viewModel.validateSurname, surname)
        validateObserver(viewModel.validatePass, password)
        validateObserver(viewModel.validatePassRepeat, repeatPassword)
        validateObserver(viewModel.validateDate, dateTv)
        login.validateObserver(this, viewModel.showButton)
        viewModel.state.observe(this, Observer {
			when (it) {
				LoginUIState.Input -> {
					/*Nothing*/
				}
				is LoginUIState.Navigate -> {
					cache.setData(it.data)
					navigate<InfoActivity>()
				}
			}
		})
    }

    private fun showDatePicker() {
        val mcurrentTime = Calendar.getInstance()
        val year = mcurrentTime.get(Calendar.YEAR)
        val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)

        DatePickerDialog(this, { _, year, month, dayOfMonth ->
			dateTv.setText(String.format("%d.%d.%d", dayOfMonth, month + 1, year))
		}, year, month, day).show()
    }
}