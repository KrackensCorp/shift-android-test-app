package ru.shiftlaboratory.shiftregistration.ui.login.uistate

import ru.shiftlaboratory.shiftregistration.data.model.User

sealed class LoginUIState {
    object Input : LoginUIState()
    data class Navigate(val data: User) : LoginUIState()
}