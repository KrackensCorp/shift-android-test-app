package ru.shiftlaboratory.shiftregistration.ui.login

import androidx.lifecycle.*
import ru.shiftlaboratory.shiftregistration.R
import ru.shiftlaboratory.shiftregistration.data.ValidateState
import ru.shiftlaboratory.shiftregistration.data.model.User
import ru.shiftlaboratory.shiftregistration.ui.login.uistate.LoginUIState

class LoginViewModel : ViewModel() {

    private val _state = MutableLiveData<LoginUIState>(LoginUIState.Input)
    val state: LiveData<LoginUIState> = _state

    val name = MutableLiveData<String>()
    val validateName = Transformations.map(name) {
        if (!isUserNameValid(it)) {
            ValidateState.Error(R.string.invalid_name)
        } else {
            ValidateState.Success
        }
    }

    val surname = MutableLiveData<String>()
    val validateSurname = Transformations.map(surname) {
        if (!isUserNameValid(it)) {
            ValidateState.Error(R.string.invalid_surname)
        } else {
            ValidateState.Success
        }
    }

    val pass = MutableLiveData<String>()
    val validatePass = Transformations.map(pass) {
        if (!isPasswordValid(it)) {
            ValidateState.Error(R.string.invalid_password)
        } else {
            ValidateState.Success
        }
    }

    val date = MutableLiveData<String>()
    val validateDate = Transformations.map(date) {
        if (it.isEmpty()) {
            ValidateState.Error(R.string.invalid_date)
        } else {
            ValidateState.Success
        }
    }

    val passRepeat = MutableLiveData<String>()
    val validatePassRepeat = Transformations.map(passRepeat) {
        if (it != pass.value) {
            ValidateState.Error(R.string.invalid_repeat_password)
        } else {
            ValidateState.Success
        }
    }


    val showButton: LiveData<Boolean> = initMediator()

    private fun initMediator(): LiveData<Boolean> {
        val liveDataMerger = MediatorLiveData<Boolean>()
        liveDataMerger.addSource(validateName) {
            liveDataMerger.value = mergerCondition()
        }
        liveDataMerger.addSource(validateSurname) {
            liveDataMerger.value = mergerCondition()
        }
        liveDataMerger.addSource(validatePass) {
            liveDataMerger.value = mergerCondition()
        }
        liveDataMerger.addSource(validatePassRepeat) {
            liveDataMerger.value = mergerCondition()
        }
        liveDataMerger.addSource(validateDate) {
            liveDataMerger.value = mergerCondition()
        }
        return liveDataMerger
    }

    private fun mergerCondition(): Boolean =
        validateName.value is ValidateState.Success
                && validateSurname.value is ValidateState.Success
                && validatePass.value is ValidateState.Success
                && validatePassRepeat.value is ValidateState.Success
                && validateDate.value is ValidateState.Success

    fun login() {
        _state.value = LoginUIState.Navigate(
            User(
                id = (0..200).random(),
                name = name.getOrException("Name is null"),
                surname = surname.getOrException("surname is null"),
                password = pass.getOrException("password is null"),
                date = date.getOrException("date is null")
            )
        )
    }

    private fun isUserNameValid(value: String): Boolean {
        value.toCharArray().forEach {
            if (!it.isUpperCase() && !it.isLowerCase()) {
                return false
            }
        }
        return value.length in 2..40
    }

    private fun isPasswordValid(value: String): Boolean {
        var hasNumber = 0
        var lowerCase = 0
        var upperCase = 0
        value.toCharArray().forEach {
            when {
                it.isDigit() -> hasNumber++
                it.isLowerCase() -> lowerCase++
                it.isUpperCase() -> upperCase++
            }
        }
        return hasNumber >= 1 && lowerCase >= 1 && upperCase >= 1 && value.length in 6..40
    }

    private fun <T> LiveData<T>.getOrException(message: String) =
        value ?: throw IllegalArgumentException(message)
}