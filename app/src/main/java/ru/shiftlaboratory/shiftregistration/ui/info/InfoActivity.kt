package ru.shiftlaboratory.shiftregistration.ui.info

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import ru.shiftlaboratory.shiftregistration.R
import ru.shiftlaboratory.shiftregistration.ext.SimpleCache
import ru.shiftlaboratory.shiftregistration.ext.navigate
import ru.shiftlaboratory.shiftregistration.ui.login.LoginActivity

class InfoActivity : AppCompatActivity() {

    private lateinit var viewModel: InfoViewModel

    private val cache by lazy { SimpleCache(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(InfoViewModel::class.java)
        viewModel.data = cache.getData() ?: throw IllegalArgumentException("user is null")

        helloBtn.setOnClickListener {
            showDialog()
        }

        clear.setOnClickListener {
            cache.clearData()
            navigate<LoginActivity>()
        }
    }

    private fun showDialog() {
        val message =
            "${getString(R.string.hello_alert_dialog)} ${viewModel.data.name} ${viewModel.data.surname}"
        val dialog = AlertDialog.Builder(this)
            .setTitle(R.string.welcome)
            .setMessage(message)
            .setPositiveButton(R.string.action_hello_alert_dialog) { d: DialogInterface, _: Int ->
                d.dismiss()
            }
        dialog.show()
    }
}




