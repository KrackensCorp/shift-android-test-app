package ru.shiftlaboratory.shiftregistration.data

import androidx.annotation.StringRes

sealed class ValidateState(@StringRes open val errorMessage: Int? = null) {
    data class Error(override val errorMessage: Int) : ValidateState(errorMessage)
    object Success : ValidateState()
}