package ru.shiftlaboratory.shiftregistration.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
	val id: Int,
	val name: String,
	val surname: String,
	val password: String,
	val date: String
) : Parcelable